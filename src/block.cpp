#include "block.h"

Block::Block(EntityType type, bool movable) : m_type{ type }, m_movable{ movable }
{
	sf::Vector2f default_pos{ 111.f, 111.f };
	m_position = default_pos;
}

void Block::determinePosition(const sf::Vector2i& factor)
{
	m_position.x += kTileSize * factor.x;
	m_position.y += kTileSize * factor.y;
}

void Block::setPosition(const sf::Vector2f& new_p)
{
	m_position = new_p;
}

void Block::setTexture(const sf::Texture& texture, const sf::IntRect& rect)
{
	m_sprite.setTexture(texture);
	m_sprite.setTextureRect(rect);
}

void Block::draw(sf::RenderWindow& window)
{
	// Default window size is assumed to be (kTileSize * kMapWidth, kTileSize * kMapHeight)
	// where kMapWidth = kMapHeight
	float scaleX = static_cast<float>(window.getSize().x)
		/ static_cast<float>(kTileSize * kMapWidth);
	float scaleY = static_cast<float>(window.getSize().y)
		/ static_cast<float>(kTileSize * kMapHeight);
	m_sprite.setScale(scaleX, scaleY);
	m_sprite.setPosition(m_position.x * scaleX, m_position.y * scaleY);
	window.draw(m_sprite);
}

void Block::update()
{}

const sf::IntRect BlockInfo::s_red_gem{ 367, 35, 90, 90 };
const sf::IntRect BlockInfo::s_green_gem{ 366, 148, 90, 90 };
const sf::IntRect BlockInfo::s_blue_gem{ 363, 259, 90, 90 };
const sf::IntRect BlockInfo::s_wall{ 115, 372, 90, 90 };