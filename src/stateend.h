#ifndef STATEEND_H
#define STATEEND_H

#include "basestate.h"
#include "eventmanager.h"
//-----------------------------
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>
//-----------------------------
#include <array>

using Buttons = std::array<sf::RectangleShape, 2>;
using ButtonLabels = std::array<sf::Text, 2>;
using ButtonPositions = std::array<sf::Vector2f, 2>;

class StateEnd : public BaseState
{
public:
	StateEnd(StateManager* state_mgr);
	~StateEnd() = default;

	void onCreate() override;
	void onDestroy() override;

	void activate() override;
	void deactivate() override;

	void update(const sf::Time& time) override;
	void draw() override;

	void click(EventDetails* details);
private:

	sf::Text m_text;
	sf::Vector2f m_button_size;
	ButtonPositions m_button_pos;
	Buttons m_buttons;
	ButtonLabels m_labels;
	bool m_restart;
};
#endif // !STATEEND_H

