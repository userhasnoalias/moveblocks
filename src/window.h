#ifndef WINDOW_H
#define WINDOW_H

#include "eventmanager.h"
//-----------------------------
#include <SFML/Graphics.hpp>
//-----------------------------
#include <string>

class Window
{
public:
	Window();
	Window(const sf::Vector2u& size, const std::string& title);
	~Window();

	void update();

	void toggleFullScreen(EventDetails* details);
	void close(EventDetails* details = nullptr);

	void beginDraw();
	void endDraw();
	void draw(sf::Drawable& drawable);

	bool isClosed() const { return m_closed; }
	bool isFullScreen() const { return m_fullScreen; }
	sf::Vector2u size() const { return m_size; }

	sf::RenderWindow& getRenderWindow() { return m_window; }
	EventManager& getEventManager() { return m_event_manager; }
private:
	void create();

	sf::RenderWindow m_window;
	EventManager m_event_manager;
	sf::Vector2u m_size;
	sf::Vector2u m_default_size;
	std::string m_title;
	bool m_closed{};
	bool m_fullScreen{};
};
#endif // !WINDOW_H