#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include "loadfromrc.h"
//-----------------------------
#include <SFML/Graphics.hpp>
//-----------------------------
#include <array>
#include <cstdint>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

using ResInfo = std::pair<void*, std::size_t>;
using LocationContainer = std::unordered_map<int, ResInfo>;
using IDs = std::vector<int>;

template<class Derived, class T>
class ResourceManager
{
protected:
	using ResourceContainer = std::unordered_map<int, std::pair<T*, std::size_t>>;

	ResourceManager(const IDs& id_vec, const char* type)
	{
		for (auto id : id_vec)
		{
			auto res_info = rsc::loadFromRc(id, type);
			if (!res_info.first) { continue; }
			m_locations.emplace(id, res_info);
		}
	}
	~ResourceManager() { releaseAll(); }

	T* load(ResInfo resource)
	{
		return static_cast<Derived*>(this)->load(resource);
	}

public:
	bool requireResource(int id)
	{
		auto res = find(id);
		if (res)
		{
			++res->second;
			return true;
		}

		auto loc = m_locations.find(id);
		if (loc == m_locations.end()) { return false; }
		T* resource = load(loc->second);
		if (!resource) { return false; }
		m_resources.emplace(id, std::make_pair(resource, 1u));
		return true;
	}

	bool releaseResource(int id)
	{
		auto res = find(id);
		if (!res) { return false; }
		--res->second;
		if (res->second == 0u) { unload(id); }
		return true;
	}

	void releaseAll()
	{
		while (m_resources.begin() != m_resources.end())
		{
			auto it = m_resources.begin();
			delete it->second.first;
			m_resources.erase(it);
		}
	}

	T* getResource(int id)
	{
		auto res = find(id);
		return (res ? res->first : nullptr);
	}

private:
	std::pair<T*, std::size_t>* find(int id)
	{
		auto it = m_resources.find(id);
		return (it != m_resources.end() ? &it->second : nullptr);
	}

	bool unload(int id)
	{
		auto it = m_resources.find(id);
		if (it == m_resources.end()) { return false; }
		delete it->second.first;
		m_resources.erase(it);
		return true;
	}

	LocationContainer m_locations;
	ResourceContainer m_resources;
};

#endif // !RESOURCEMANAGER_H

