#include "blockmanager.h"
#include "randomization.h"
#include "resource.h"
#include "sharedcontext.h"
#include "statemanager.h"

BlockManager::BlockManager(SharedContext* context) : m_context{ context }, m_field{ this }, 
	m_win{ false }
{
	TextureManager* texture_mgr = m_context->m_texture_manager;
	texture_mgr->requireResource(ID_BG);
	texture_mgr->requireResource(ID_GEM);

	do
	{
		m_blocks.clear();
		m_top_blocks.clear();
		initBlocks();
		initTopBlocks();
		checkForWin();
	} while (m_win);

	EventManager* ev_mgr = m_context->m_event_manager;
	ev_mgr->addCallback(StateType::Game, "MoveLeft", &BlockManager::moveLeft, this);
	ev_mgr->addCallback(StateType::Game, "MoveRight", &BlockManager::moveRight, this);
	ev_mgr->addCallback(StateType::Game, "MoveUp", &BlockManager::moveUp, this);
	ev_mgr->addCallback(StateType::Game, "MoveDown", &BlockManager::moveDown, this);
	ev_mgr->addCallback(StateType::Game, "MouseLeft", &BlockManager::selectBlock, this);
}

BlockManager::~BlockManager()
{
	TextureManager* texture_mgr = m_context->m_texture_manager;
	texture_mgr->releaseResource(ID_BG);
	texture_mgr->releaseResource(ID_GEM);

	EventManager* ev_mgr = m_context->m_event_manager;
	ev_mgr->removeCallback(StateType::Game, "MoveLeft");
	ev_mgr->removeCallback(StateType::Game, "MoveRight");
	ev_mgr->removeCallback(StateType::Game, "MoveUp");
	ev_mgr->removeCallback(StateType::Game, "MoveDown");
	ev_mgr->removeCallback(StateType::Game, "MouseLeft");
}

void BlockManager::selectBlock(EventDetails* details)
{
	sf::Vector2f mouse_pos = static_cast<sf::Vector2f>(details->m_mouse);
	for (auto& block : m_blocks)
	{
		if (block.getSprite().getGlobalBounds().contains(mouse_pos))
		{
			if (block.getType() != EntityType::Wall)
			{
				m_selected_block = &block;
				return;
			}
		}
	}

	m_selected_block = nullptr;
}

void BlockManager::draw()
{
	sf::RenderWindow& window = m_context->m_window->getRenderWindow();
	for (auto& b : m_top_blocks)
	{
		b.draw(window);
	}

	for (auto& b : m_blocks)
	{
		b.draw(window);
	}
}

void BlockManager::update()
{}

void BlockManager::moveLeft([[maybe_unused]] EventDetails* details)
{
	if (m_selected_block != nullptr)
	{
		m_field.moveLeft();
		checkForWin();
	}
}

void BlockManager::moveRight([[maybe_unused]] EventDetails* details)
{
	if (m_selected_block != nullptr)
	{
		m_field.moveRight();
		checkForWin();
	}
}

void BlockManager::moveUp([[maybe_unused]] EventDetails* details)
{
	if (m_selected_block != nullptr)
	{
		m_field.moveUp();
		checkForWin();
	}	
}

void BlockManager::moveDown([[maybe_unused]] EventDetails* details)
{
	if (m_selected_block != nullptr)
	{
		m_field.moveDown();
		checkForWin();
	}
}

void BlockManager::checkForWin()
{
	for (int i = 0, x = 0; i < m_top_blocks.size(); ++i)
	{
		x = static_cast<int>(m_top_blocks[i].getPosition().x) / kTileSize - 1;
		for (int j = 0; j < kRows; ++j)
		{
			if (m_top_blocks[i].getType() != m_field.getField()[x + j * kRows].second)
			{
				m_win = false;
				return;
			}
		}
	}

	m_win = true;
}

void BlockManager::initBlocks()
{
	createBlocks(EntityType::RedGem, ID_GEM, kEachGemCount);
	createBlocks(EntityType::GreenGem, ID_GEM, kEachGemCount);
	createBlocks(EntityType::BlueGem, ID_GEM, kEachGemCount);
	createBlocks(EntityType::Wall, ID_BG, kWallCount, false);

	std::vector<sf::Vector2i> gem_factors;
	for (int y = 0; y < kRows; ++y)
	{
		for (int x = 0; x < kCols; x += 2)
		{
			gem_factors.push_back({ x, y });
		}
	}
	shuffleVec(gem_factors);

	std::vector<sf::Vector2i> wall_factors;
	for (int x = 1; x < kCols; x += 2)
	{
		for (int y = 0; y < kRows; y += 2)
		{
			wall_factors.push_back({ x, y });
		}
	}

	for (auto& b : m_blocks)
	{
		if (b.getType() == EntityType::Wall)
		{
			b.determinePosition(wall_factors.back());
			wall_factors.pop_back();
		}
		else
		{
			b.determinePosition(gem_factors.back());
			gem_factors.pop_back();
		}
		m_field.placeBlock(b);
	}
}

void BlockManager::initTopBlocks()
{
	TextureManager* text_mgr = m_context->m_texture_manager;

	m_top_blocks.emplace_back(EntityType::RedGem, false);
	m_top_blocks.emplace_back(EntityType::GreenGem, false);
	m_top_blocks.emplace_back(EntityType::BlueGem, false);

	std::vector<sf::Vector2i> top_gem_factors;
	for (int i = 0; i < kCols; i += 2)
	{
		top_gem_factors.push_back({ i, 0 });
	}
	shuffleVec(top_gem_factors);

	sf::Texture* texture = text_mgr->getResource(ID_GEM);
	for (auto& it : m_top_blocks)
	{
		it.setTexture(*texture, BlockInfo::getDefaultRect(it.getType()));
		it.setPosition(sf::Vector2f(kTileSize + kOffset, kOffset)); // default position
		it.determinePosition(top_gem_factors.back());
		top_gem_factors.pop_back();
	}
}

void BlockManager::createBlocks(EntityType type, int res_id, int count, bool movable)
{
	TextureManager* text_mgr = m_context->m_texture_manager;
	sf::Texture* texture = text_mgr->getResource(res_id);
	for (int i = 0; i < count; ++i)
	{
		m_blocks.emplace_back(type, movable);
		m_blocks.back().setTexture(*texture, BlockInfo::getDefaultRect(type));
	}
}