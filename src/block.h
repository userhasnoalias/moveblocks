#ifndef BLOCK_H
#define BLOCK_H

#include "constants.h"
//-----------------------------
#include <SFML/Graphics.hpp>
//-----------------------------
#include <iostream>
#include <utility>
#include <vector>

enum class EntityType
{
	RedGem,
	GreenGem,
	BlueGem,
	Free,
	Wall,
};

class BlockInfo
{
public:
	static const sf::IntRect& getDefaultRect(EntityType type)
	{
		switch (type)
		{
		case EntityType::RedGem:
			return s_red_gem;
		case EntityType::GreenGem:
			return s_green_gem;
		case EntityType::BlueGem:
			return s_blue_gem;
		case EntityType::Wall:
			return s_wall;
		default:
			return s_red_gem;
		}
	}
private:
	static const sf::IntRect s_red_gem;
	static const sf::IntRect s_green_gem;
	static const sf::IntRect s_blue_gem;
	static const sf::IntRect s_wall;
};

class Block
{
public:
	Block(EntityType type, bool movable = true);
	~Block() = default;

	void determinePosition(const sf::Vector2i& factor);
	void setPosition(const sf::Vector2f& new_p);
	void setTexture(const sf::Texture& texture, const sf::IntRect& rect);
	
	void draw(sf::RenderWindow& window);
	void update();

	sf::Sprite& getSprite() { return m_sprite; }
	const sf::Vector2f& getPosition() const { return m_position; }
	bool isMovable() const { return m_movable; }
	EntityType getType() const { return m_type; }

private:
	sf::Vector2f m_position;
	sf::Sprite m_sprite;
	bool m_movable{};
	EntityType m_type{};
};

#endif // !BLOCK_H