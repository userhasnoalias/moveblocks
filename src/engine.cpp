#include "engine.h"
//-----------------------------

Engine::Engine() : m_window{ {728, 728}, "Move Blocks" }, m_state_manager{ &m_context }
{
	m_clock.restart();

	m_context.m_window = &m_window;
	m_context.m_event_manager = &m_window.getEventManager();
	m_context.m_texture_manager = &m_texture_manager;
	m_context.m_font_manager = &m_font_manager;

	m_state_manager.switchTo(StateType::Game);
}

void Engine::update()
{
	m_window.update();
	m_state_manager.update(m_elapsed);
}

void Engine::render()
{
	m_window.beginDraw();
	m_state_manager.draw();
	m_window.endDraw();
}

void Engine::lateUpdate()
{
	m_state_manager.processRemovals();
	restartClock();
}