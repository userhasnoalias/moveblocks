#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include "basestate.h"
#include "sharedcontext.h"
#include "stateend.h"
#include "stategame.h"
#include "staterestart.h"
//include every state.h
//-----------------------------
#include <SFML/Graphics.hpp>
//-----------------------------
#include <functional>
#include <tuple>
#include <unordered_map>
#include <vector>

enum class StateType
{
	Global,
	Intro,
	MainMenu,
	Options,
	Game,
	Restart,
	EndGame
};

using StateContainer = std::vector<std::pair<StateType, BaseState*>>;
using TypeContainer = std::vector<StateType>;
using StateFactory = std::unordered_map<StateType, std::function<BaseState* (void)>>;

class StateManager
{
public:
	StateManager(SharedContext* context);
	~StateManager();

	void processRemovals();

	void update(const sf::Time& time);
	void draw();

	void switchTo(StateType type);
	void addToRemove(StateType type);

	SharedContext* getContext() { return m_context; }
private:
	void createState(StateType type);
	void removeState(StateType type);

	template<class T>
	void registerState(StateType type)
	{
		m_factory[type] = [this]() -> BaseState* { return new T(this); };
	}

	SharedContext* m_context;
	StateContainer m_states;
	TypeContainer m_to_remove;
	StateFactory m_factory;
};
#endif // !STATEMANAGER_H