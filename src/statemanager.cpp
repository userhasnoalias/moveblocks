#include "statemanager.h"

StateManager::StateManager(SharedContext* context) : m_context{ context }
{
	registerState<StateGame>(StateType::Game);
	registerState<StateEnd>(StateType::EndGame);
	registerState<StateRestart>(StateType::Restart);
}

StateManager::~StateManager()
{
	for (auto& state : m_states)
	{
		state.second->onDestroy();
		delete state.second;
	}
}

void StateManager::processRemovals()
{
	while (m_to_remove.begin() != m_to_remove.end())
	{
		removeState(*m_to_remove.begin());
		m_to_remove.erase(m_to_remove.begin());
	}
}

void StateManager::update(const sf::Time& time)
{
	if (m_states.empty()) { return; }

	if (m_states.back().second->isTranscendent() && m_states.size() > 1)
	{
		auto it = m_states.end();
		while (it != m_states.begin())
		{
			if (it != m_states.end())
			{
				if (!it->second->isTranscendent())
				{
					break;
				}
			}
			--it;
		}

		for (; it != m_states.end(); ++it)
		{
			it->second->update(time);
		}
	}
	else
	{
		m_states.back().second->update(time);
	}
}

void StateManager::draw()
{
	if (m_states.empty()) { return; }

	if (m_states.back().second->isTransparent() && m_states.size() > 1)
	{
		auto it = m_states.end();
		while (it != m_states.begin())
		{
			if (it != m_states.end())
			{
				if (!it->second->isTransparent())
				{
					break;
				}
			}
			--it;
		}

		for (; it != m_states.end(); ++it)
		{
			it->second->draw();
		}
	}
	else
	{
		m_states.back().second->draw();
	}
}

void StateManager::switchTo(StateType type)
{
	m_context->m_event_manager->setCurrentState(type);

	for (auto it = m_states.begin(); it != m_states.end(); ++it)
	{
		if (it->first == type)
		{
			it->second->deactivate();
			StateType tmp_state = it->first;
			BaseState* tmp_base = it->second;
			m_states.erase(it);
			m_states.emplace_back(tmp_state, tmp_base);
			m_states.back().second->activate();
			return;
		}
	}

	if (!m_states.empty()) { m_states.back().second->deactivate(); }
	createState(type);
	m_states.back().second->activate();
}

void StateManager::addToRemove(StateType type)
{
	m_to_remove.push_back(type);
}

void StateManager::createState(StateType type)
{
	auto it = m_factory.find(type);
	if (it != m_factory.end())
	{
		BaseState* state = it->second();
		m_states.emplace_back(type, state);
		m_states.back().second->onCreate();
	}
}

void StateManager::removeState(StateType type)
{
	for (auto it = m_states.begin(); it != m_states.end(); ++it)
	{
		if (it->first == type)
		{
			it->second->onDestroy();
			delete it->second;
			m_states.erase(it);
			return;
		}
	}
}