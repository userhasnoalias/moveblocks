#include "constants.h"
#include "stateend.h"
#include "statemanager.h"
//-----------------------------
#include <string>

StateEnd::StateEnd(StateManager* state_mgr) : BaseState{ state_mgr }, m_restart{ false }
{}

void StateEnd::onCreate()
{
	setTransparent(true);

	EventManager* ev_mgr = m_state_manager->getContext()->m_event_manager;
	ev_mgr->addCallback(StateType::EndGame, "MouseLeft", &StateEnd::click, this);

	FontManager* font_mgr = m_state_manager->getContext()->m_font_manager;
	font_mgr->requireResource(ID_CYR);
	m_text.setFont(*font_mgr->getResource(ID_CYR));
	m_text.setString("  You've won! Congrats!\nDo you wish to play again?");
	m_text.setCharacterSize(60);
	m_text.setFillColor(sf::Color::Magenta);
	sf::FloatRect rect = m_text.getLocalBounds();
	m_text.setOrigin(rect.left + rect.width / 2.f, rect.top + rect.height / 2.f);

	m_button_size = { kTileSize * 2.f, kTileSize * 1.f };
	m_button_pos = { 
		sf::Vector2f{kTileSize * 1.f, kTileSize * 3.f}, 
		sf::Vector2f{kTileSize * 4.f, kTileSize * 3.f} };

	std::string text[2] = { "YES", "NO" };

	font_mgr->requireResource(ID_AR);
	for (std::size_t i = 0; i < m_buttons.size(); ++i)
	{
		m_buttons[i].setSize(m_button_size);
		m_buttons[i].setFillColor(sf::Color{255, 0, 0, 200});

		m_labels[i].setFont(*font_mgr->getResource(ID_AR));
		m_labels[i].setString(text[i]);
		m_labels[i].setCharacterSize(30);
		m_labels[i].setFillColor(sf::Color::White);
		
		sf::FloatRect textrect = m_labels[i].getLocalBounds();
		m_labels[i].setOrigin(textrect.left + textrect.width / 2.f, textrect.top + textrect.height / 2.f);
	}
}

void StateEnd::onDestroy()
{
	EventManager* ev_mgr = m_state_manager->getContext()->m_event_manager;
	ev_mgr->removeCallback(StateType::EndGame, "MouseLeft");

	FontManager* font_mgr = m_state_manager->getContext()->m_font_manager;
	font_mgr->releaseResource(ID_CYR);
	font_mgr->releaseResource(ID_AR);
}

void StateEnd::draw()
{
	sf::RenderWindow& window = m_state_manager->getContext()->m_window->getRenderWindow();
	sf::Vector2u wnd_size = m_state_manager->getContext()->m_window->size();
	sf::Vector2f scale = static_cast<sf::Vector2f>(wnd_size)
		/ static_cast<float>(kTileSize * kMapWidth);

	m_text.setPosition(wnd_size.x / 2.f, wnd_size.y / 4.f);
	m_text.setScale(scale);
	window.draw(m_text);
	
	for (std::size_t i = 0; i < m_buttons.size(); ++i)
	{
		m_buttons[i].setPosition(m_button_pos[i].x * scale.x, m_button_pos[i].y * scale.y);
		m_buttons[i].setScale(scale);
		m_labels[i].setScale(scale);
		sf::FloatRect rect = m_buttons[i].getGlobalBounds();
		m_labels[i].setPosition(rect.left + rect.width / 2.f, rect.top + rect.height / 2.f);

		window.draw(m_buttons[i]);
		window.draw(m_labels[i]);
	}
}

void StateEnd::click(EventDetails* details)
{
	sf::Vector2f mouse_pos = static_cast<sf::Vector2f>(details->m_mouse);
	for (std::size_t i = 0; i < m_buttons.size(); ++i)
	{
		if (m_buttons[i].getGlobalBounds().contains(mouse_pos))
		{
			if (i == 0)
			{
				m_restart = true;
			}
			else if (i == 1)
			{
				m_state_manager->getContext()->m_window->close();
			}
		}
	}
}

void StateEnd::update([[maybe_unused]] const sf::Time& time)
{
	if (m_restart)
	{
		m_state_manager->addToRemove(StateType::Game);
		m_state_manager->addToRemove(StateType::EndGame);
		m_state_manager->switchTo(StateType::Restart);
	}
}

void StateEnd::activate()
{}

void StateEnd::deactivate()
{}