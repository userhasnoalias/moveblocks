#ifndef BASESTATE_H
#define BASESTATE_H

#include <SFML/Graphics.hpp>

class StateManager;

class BaseState
{
public:
	BaseState(StateManager* state_mgr) : m_state_manager{ state_mgr }, m_transparent{ false },
		m_transcendent{ false }
	{}
	virtual ~BaseState() = default;

	virtual void onCreate() = 0;
	virtual void onDestroy() = 0;

	virtual void activate() = 0;
	virtual void deactivate() = 0;

	virtual void update(const sf::Time& time) = 0;
	virtual void draw() = 0;

	void setTransparent(bool value) { m_transparent = value; }
	bool isTransparent() const { return m_transparent; }
	void setTranscendent(bool value) { m_transcendent = value; }
	bool isTranscendent() const { return m_transcendent; }
	StateManager* getStateManager() { return m_state_manager; }
protected:
	StateManager* m_state_manager;
	bool m_transparent{};
	bool m_transcendent{};
};
#endif // !BASESTATE_H
