#ifndef ENGINE_H
#define ENGINE_H

#include "eventmanager.h"
#include "statemanager.h"
#include "texturemanager.h"
#include "window.h"
//-----------------------------
#include <SFML/Graphics.hpp>
//-----------------------------

class Engine
{
public:
	Engine();

	void update();
	void render();
	void lateUpdate();

	sf::Time getElapsed() const { return m_clock.getElapsedTime(); }
	Window& getWindow() { return m_window; }
private:
	void restartClock() { m_elapsed = m_clock.restart(); }

	sf::Clock m_clock;
	sf::Time m_elapsed;
	SharedContext m_context;
	Window m_window;
	TextureManager m_texture_manager;
	FontManager m_font_manager;
	StateManager m_state_manager;
};
#endif // !ENGINE_H