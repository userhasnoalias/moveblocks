#ifndef SHAREDCONTEXT_H
#define SHAREDCONTEXT_H

#include "eventmanager.h"
#include "fontmanager.h"
#include "texturemanager.h"
#include "window.h"

class Map;

class SharedContext
{
public:
	SharedContext() : m_event_manager{ nullptr }, m_window{ nullptr },
		m_texture_manager{ nullptr }, m_map{ nullptr }, m_font_manager{ nullptr }
	{}

	EventManager* m_event_manager;
	TextureManager* m_texture_manager;
	FontManager* m_font_manager;
	Window* m_window;
	Map* m_map;
};
#endif // !SHAREDCONTEXT_H

