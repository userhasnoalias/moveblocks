#ifndef STATERESTART_H
#define STATERESTART_H

#include "basestate.h"
//-----------------------------

class StateRestart : public BaseState
{
public:
	StateRestart(StateManager* state_mgr);
	~StateRestart() = default;

	void onCreate() override;
	void onDestroy() override;

	void activate() override;
	void deactivate() override;

	void update(const sf::Time& time) override;
	void draw() override;
private:
};
#endif // !STATERESTART_H

