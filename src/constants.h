#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <cstdlib>

// Defines how many blocks of each type exist
constexpr inline int kEachGemCount = 5;
constexpr inline int kWallCount = 6;
constexpr inline int kMaxGems = 15;

constexpr inline std::size_t kTileSize = 104;

// Offset x and y from each top-left tile corner
constexpr inline float kOffset = 7.f;

// Map size
constexpr inline int kMapWidth = 7;
constexpr inline int kMapHeight = 7;

// Size of GameField
constexpr inline int kRows = 5;
constexpr inline int kCols = 5;
constexpr inline int fieldSize = kRows * kCols;

#endif // !CONSTANTS_H

