#include "stategame.h"
#include "statemanager.h"

StateGame::StateGame(StateManager* state_mgr) : BaseState{ state_mgr },
	m_block_manager{ state_mgr->getContext() }, m_map{ nullptr }
{}

void StateGame::onCreate()
{
	m_map = new Map(m_state_manager->getContext());
}

void StateGame::onDestroy()
{
	delete m_map;
	m_map = nullptr;
}

void StateGame::draw()
{
	m_map->draw();
	m_block_manager.draw();
}

void StateGame::update([[maybe_unused]] const sf::Time& time)
{
	if (m_block_manager.hasWon())
		m_state_manager->switchTo(StateType::EndGame);
}

void StateGame::activate()
{}

void StateGame::deactivate()
{}