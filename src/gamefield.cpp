#include "blockmanager.h"
#include "gamefield.h"
#include "map.h"
#include "sharedcontext.h"
//-----------------------------
#include <utility>

GameField::GameField(BlockManager* b_mgr) : m_block_manager{ b_mgr }
{
	float tile_size = static_cast<float>(m_block_manager->getContext()->m_map->getTileSize());
	sf::Vector2f start_pos = { tile_size + kOffset, tile_size + kOffset };
	for (int i = 0; i < kRows; ++i)
	{
		for (int j = 0; j < kCols; ++j)
		{
			sf::Vector2f offset = { tile_size * j, tile_size * i };
			m_field[i * kRows + j] = std::make_pair(start_pos + offset, EntityType::Free);
		}
	}
}

void GameField::placeBlock(Block& b)
{
	for (auto& it : m_field)
	{
		if (it.first == b.getPosition())
			it.second = b.getType();
	}
}

int GameField::find(const sf::Vector2f& coords)
{
	for (std::size_t i = 0; i < m_field.size(); ++i)
	{
		if (m_field[i].first == coords)
			return i;
	}

	return -1;
}

void GameField::moveLeft()
{
	Block* current = m_block_manager->getSelectedBlock();
	int index = find(current->getPosition());
	if (index != -1 && current->isMovable())
	{
		if ((index % kCols - 1 >= 0) && m_field[index - 1].second == EntityType::Free)
		{
			std::swap(m_field[index - 1].second, m_field[index].second);
			current->setPosition(m_field[index - 1].first);
		}
	}
}

void GameField::moveRight()
{
	Block* current = m_block_manager->getSelectedBlock();
	int index = find(current->getPosition());
	if (index != -1 && current->isMovable())
	{
		if ((index % kCols + 1 < kCols) && m_field[index + 1].second == EntityType::Free)
		{
			std::swap(m_field[index + 1].second, m_field[index].second);
			current->setPosition(m_field[index + 1].first);
		}
	}
}

void GameField::moveUp()
{
	Block* current = m_block_manager->getSelectedBlock();
	int index = find(current->getPosition());
	if (index != -1 && current->isMovable())
	{
		if ((index - kRows >= 0) && m_field[index - kRows].second == EntityType::Free)
		{
			std::swap(m_field[index - kRows].second, m_field[index].second);
			current->setPosition(m_field[index - kRows].first);
		}
	}
}

void GameField::moveDown()
{
	Block* current = m_block_manager->getSelectedBlock();
	int index = find(current->getPosition());
	if (index != -1 && current->isMovable())
	{
		if ((index + kRows < fieldSize) && m_field[index + kRows].second == EntityType::Free)
		{
			std::swap(m_field[index + kRows].second, m_field[index].second);
			current->setPosition(m_field[index + kRows].first);
		}
	}
}