#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include "resource.h"
#include "resourcemanager.h"
//-----------------------------
#include <SFML/Graphics/Texture.hpp>
//-----------------------------
#include <iostream>

class TextureManager : public ResourceManager<TextureManager, sf::Texture>
{
public:
	TextureManager() : ResourceManager{ {ID_BG, ID_GEM}, "TEXTURE" }
	{}

	sf::Texture* load(ResInfo resource)
	{
		sf::Texture* texture = new sf::Texture{};
		if (!texture->loadFromMemory(resource.first, resource.second))
		{
			delete texture;
			texture = nullptr;
			std::cerr << "Failed loading texture\n";
		}

		return texture;
	}
};
#endif // !TEXTUREMANAGER_H

