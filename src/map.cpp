#include "map.h"
#include "resource.h"

Map::Map(SharedContext* context) : m_context{ context }
{
	createDefaultMap();
}

Map::~Map()
{
	TextureManager* texture_mgr = m_context->m_texture_manager;
	texture_mgr->releaseResource(ID_BG);
}

void Map::createDefaultMap()
{
	TextureManager* texture_mgr = m_context->m_texture_manager;
	texture_mgr->requireResource(ID_BG);
	m_map_sprite.setTexture(*texture_mgr->getResource(ID_BG));

	m_map = {
		"0000000",
		"0     0",
		"0     0",
		"0     0",
		"0     0",
		"0     0",
		"0000000"
	};
}

void Map::draw()
{
	for (int i = 0; i < kMapHeight; ++i)
	{
		for (int j = 0; j < kMapWidth; ++j)
		{
			if (m_map[i][j] == ' ') m_map_sprite.setTextureRect(sf::IntRect(15, 18, 104, 104));
			else if (m_map[i][j] == '0') m_map_sprite.setTextureRect(sf::IntRect(381, 433, 104, 104));

			// If map has no square form then we will need to count scale both for x and y axes
			// Default is 7x7 so we can count only for one axis
			sf::Vector2f scale = static_cast<sf::Vector2f>(m_context->m_window->size()) 
				/ static_cast<float>(kTileSize * kMapWidth);
			m_map_sprite.setScale(scale);
			m_map_sprite.setPosition(j * (kTileSize * scale.x), i * (kTileSize * scale.y));
			m_context->m_window->getRenderWindow().draw(m_map_sprite);
		}
	}
}