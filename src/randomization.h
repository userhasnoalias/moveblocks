#ifndef RANDOMIZATION_H
#define RANDOMIZATION_H

#include <SFML/Graphics.hpp>
//-----------------------------
#include <vector>

void shuffleVec(std::vector<sf::Vector2i>& vec);

#endif // !RANDOMIZATION_H

