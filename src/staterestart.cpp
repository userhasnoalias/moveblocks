#include "statemanager.h"
#include "staterestart.h"

StateRestart::StateRestart(StateManager* state_mgr) : BaseState{ state_mgr }
{}

void StateRestart::onCreate()
{}

void StateRestart::onDestroy()
{}

void StateRestart::draw()
{}

void StateRestart::activate()
{}

void StateRestart::deactivate()
{}

void StateRestart::update([[maybe_unused]] const sf::Time& time)
{
	m_state_manager->addToRemove(StateType::Restart);
	m_state_manager->switchTo(StateType::Game);
}