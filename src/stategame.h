#ifndef STATEGAME_H
#define STATEGAME_H

#include "basestate.h"
#include "block.h"
#include "blockmanager.h"
#include "eventmanager.h"
#include "map.h"
//-----------------------------
#include <vector>

using BlocksContainer = std::vector<Block>;

class StateGame : public BaseState
{
public:
	StateGame(StateManager* state_mgr);
	~StateGame() = default;

	void onCreate() override;
	void onDestroy() override;

	void activate() override;
	void deactivate() override;

	void update(const sf::Time& time) override;
	void draw() override;
private:
	Map* m_map;
	BlockManager m_block_manager;
};
#endif // !STATEGAME_H

