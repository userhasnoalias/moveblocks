#ifndef MAP_H
#define MAP_H

#include "constants.h"
#include "sharedcontext.h"
//-----------------------------
#include <SFML/Graphics.hpp>
//-----------------------------
#include <array>
#include <string>

using MapPattern = std::array<std::string, kMapHeight>;

class Map
{
public:
	Map(SharedContext* context);
	~Map();

	void draw();

	sf::Sprite& getSprite() { return m_map_sprite; }
	const MapPattern& getMapPattern() const { return m_map; }
	std::size_t getTileSize() const { return kTileSize; }
private:
	void createDefaultMap();

	SharedContext* m_context;
	MapPattern m_map;
	sf::Sprite m_map_sprite;
};
#endif // !MAP_H

