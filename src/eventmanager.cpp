#include "eventmanager.h"
#include "statemanager.h"

EventManager::EventManager() : m_current_state{ StateType::Global }, m_has_focus{ true }
{
	std::vector<Binding*> binds{};
	binds.push_back(new Binding("MoveLeft", EventType::KeyDown, sf::Keyboard::Key::A));
	binds.push_back(new Binding("MoveRight", EventType::KeyDown, sf::Keyboard::Key::D));
	binds.push_back(new Binding("MoveUp", EventType::KeyDown, sf::Keyboard::Key::W));
	binds.push_back(new Binding("MoveDown", EventType::KeyDown, sf::Keyboard::Key::S));
	binds.push_back(new Binding("ToggleFullScreen", EventType::KeyDown, sf::Keyboard::Key::F5));
	binds.push_back(new Binding("MouseLeft", EventType::MouseButtonDown, sf::Mouse::Left));
	binds.push_back(new Binding("CloseWindow", EventType::Closed));

	for (auto bind : binds)
	{
		if (!addBinding(bind)) { delete bind; }
	}
}

EventManager::~EventManager()
{
	for (auto& bind : m_bindings)
	{
		delete bind.second;
	}
}

bool EventManager::addBinding(Binding* bind)
{
	return m_bindings.emplace(bind->m_name, bind).second;
}

bool EventManager::removeBinding(const std::string& name)
{
	auto it = m_bindings.find(name);
	if (it == m_bindings.end()) { return false; }

	delete it->second;
	m_bindings.erase(it);
	return true;
}

void EventManager::processEvent(sf::Event& event)
{
	if (!m_has_focus) { return; }

	for (auto& b_it : m_bindings)
	{
		Binding* bind = b_it.second;
		for (auto& ev_it : bind->m_events)
		{
			EventType sfml_event = static_cast<EventType>(event.type);
			if (sfml_event != ev_it.first) { continue; }

			if (sfml_event == EventType::KeyDown || sfml_event == EventType::KeyUp)
			{
				if (ev_it.second.m_code == event.key.code)
				{
					bind->m_hit_counter += 1;
				}
			}
			else if (sfml_event == EventType::MouseButtonDown || sfml_event == EventType::MouseButtonUp)
			{
				if (ev_it.second.m_code == event.key.code)
				{
					bind->m_details.m_mouse.x = event.mouseButton.x;
					bind->m_details.m_mouse.y = event.mouseButton.y;

					bind->m_hit_counter += 1;
				}
			}
			else
			{
				if (sfml_event == EventType::WindowResized)
				{
					bind->m_details.m_size.x = event.size.width;
					bind->m_details.m_size.y = event.size.height;
				}

				bind->m_hit_counter += 1;
			}
		}
	}
}

void EventManager::update()
{
	if (!m_has_focus) { return; }

	for (auto& b_it : m_bindings)
	{
		Binding* bind = b_it.second;
		for (auto& ev_it : b_it.second->m_events)
		{
			switch (ev_it.first)
			{
			case EventType::Keyboard:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(ev_it.second.m_code)))
				{
					bind->m_hit_counter += 1;
				}
				break;
			case EventType::Mouse:
				if (sf::Mouse::isButtonPressed(sf::Mouse::Button(ev_it.second.m_code)))
				{
					bind->m_hit_counter += 1;
				}
				break;
			default:
				break;
			}
		}

		if (bind->m_hit_counter == bind->m_events.size())
		{
			auto stateCallbacks = m_callbacks.find(m_current_state);
			if (stateCallbacks != m_callbacks.end())
			{
				auto it = stateCallbacks->second.find(bind->m_name);
				if (it != stateCallbacks->second.end())
				{
					it->second(&bind->m_details);
				}
			}

			auto globalCallbacks = m_callbacks.find(StateType::Global);
			if (globalCallbacks != m_callbacks.end())
			{
				auto it = globalCallbacks->second.find(bind->m_name);
				if (it != globalCallbacks->second.end())
				{
					it->second(&bind->m_details);
				}
			}
		}

		bind->m_hit_counter = 0;
		bind->m_details.clear();
	}
}