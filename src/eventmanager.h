#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include <SFML/Graphics.hpp>
//-----------------------------
#include <functional>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

enum class EventType;
enum class StateType;
class EventCode;
class EventDetails;
class Binding;

using Events = std::vector<std::pair<EventType, EventCode>>;
using Bindings = std::unordered_map<std::string, Binding*>;
using CallbackContainer = std::unordered_map<std::string, std::function<void(EventDetails*)>>;
// Every state has its own Callbacks
using Callbacks = std::unordered_map<StateType, CallbackContainer>;

enum class EventType
{
	KeyDown = sf::Event::KeyPressed,
	KeyUp = sf::Event::KeyReleased,
	MouseButtonDown = sf::Event::MouseButtonPressed,
	MouseButtonUp = sf::Event::MouseButtonReleased,
	Closed = sf::Event::Closed,
	WindowResized = sf::Event::Resized,
	GainedFocus = sf::Event::GainedFocus,
	LostFocus = sf::Event::LostFocus,
	Keyboard = sf::Event::Count + 1, Mouse, MaxEventType
};

class EventCode
{
public:
	EventCode() : m_code{ 0 } {}
	EventCode(int code) : m_code{ code } {}

	int m_code;
};

class EventDetails
{
public:
	EventDetails(const std::string& name) : m_name{ name }
	{
		clear();
	}

	void clear()
	{
		m_mouse = { 0, 0 };
		m_size = { 0, 0 };
	}
	
	std::string m_name;
	sf::Vector2i m_mouse;
	sf::Vector2i m_size;
};

class Binding
{
public:
	Binding(const std::string& name, EventType type, EventCode code = EventCode{})
		: m_name{ name }, m_details{ name }, m_hit_counter{ 0 }
	{
		bindEvent(type, code);
	}

	void bindEvent(EventType type, EventCode code = EventCode{})
	{
		m_events.emplace_back(type, code);
	}

	Events m_events;
	std::string m_name;
	int m_hit_counter;
	EventDetails m_details;
};

class EventManager
{
public:
	EventManager();
	~EventManager();

	bool addBinding(Binding* bind);
	bool removeBinding(const std::string& name);

	void update();
	void processEvent(sf::Event& event);

	void setCurrentState(StateType state) { m_current_state = state; }
	void setFocus(bool focus) { m_has_focus = focus; }

	template<class T>
	bool addCallback(StateType type, const std::string& name, void(T::*func)(EventDetails*), 
		T* instance)
	{
		auto it = m_callbacks.emplace(type, CallbackContainer{}).first;
		auto fun = std::bind(func, instance, std::placeholders::_1);
		return it->second.emplace(name, fun).second;
	}

	bool removeCallback(StateType type, const std::string& name)
	{
		auto it = m_callbacks.find(type);
		if (it == m_callbacks.end()) { return false; }

		auto it2 = it->second.find(name);
		if (it2 == it->second.end()) { return false; }
		it->second.erase(it2);
		return true;
	}
private:
	StateType m_current_state;
	Bindings m_bindings;
	Callbacks m_callbacks;
	bool m_has_focus;
};
#endif // !EVENTMANAGER_H