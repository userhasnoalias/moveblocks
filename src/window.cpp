#include "statemanager.h"
#include "window.h"

Window::Window() : Window{{704u, 704u}, "Move Blocks"}
{}

Window::Window(const sf::Vector2u& size, const std::string& title)
	: m_size{ size }, m_title{ title }, m_closed{ false }, m_fullScreen{ false }
{
	m_default_size = size;
	m_event_manager.addCallback(StateType::Global, "CloseWindow", &Window::close, this);
	m_event_manager.addCallback(StateType::Global, "ToggleFullScreen", &Window::toggleFullScreen, this);

	create();
}

Window::~Window()
{
	m_window.close();
}

void Window::create()
{
	auto not_resizable = sf::Style::Titlebar | sf::Style::Close;
	auto style = m_fullScreen ? sf::Style::Fullscreen : not_resizable;

	if (style == not_resizable)
	{
		m_window.create({ m_default_size.x, m_default_size.y }, m_title, style);
		m_size = m_default_size;
	}
	else if (style == sf::Style::Fullscreen)
	{
		sf::VideoMode mode = sf::VideoMode::getFullscreenModes().front();
		m_window.create(mode, m_title, style);
		m_size.x = mode.width;
		m_size.y = mode.height;
	}
}

void Window::update()
{
	sf::Event event{};
	while (m_window.pollEvent(event))
	{
		if (event.type == sf::Event::GainedFocus)
		{
			m_event_manager.setFocus(true);
		}
		else if (event.type == sf::Event::LostFocus)
		{
			m_event_manager.setFocus(false);
		}
		m_event_manager.processEvent(event);
	}

	m_event_manager.update();
}

void Window::toggleFullScreen([[maybe_unused]] EventDetails* details)
{
	m_fullScreen = !m_fullScreen;
	m_window.close();
	create();
}

void Window::close([[maybe_unused]] EventDetails* details)
{
	m_closed = true;
}

void Window::beginDraw()
{
	m_window.clear();
}

void Window::endDraw()
{
	m_window.display();
}

void Window::draw(sf::Drawable& drawable)
{
	m_window.draw(drawable);
}