#ifndef GAMEFIELD_H
#define GAMEFIELD_H

#include "block.h"
#include "constants.h"
//-----------------------------
#include <SFML/System.hpp>
//-----------------------------
#include <array>
#include <tuple>

class BlockManager;

// 5x5 matrix of pairs: position coordinates and type of block
using Field = std::array<std::pair<sf::Vector2f, EntityType>, fieldSize>;

class GameField
{
public:
	GameField(BlockManager* b_manager);
	void placeBlock(Block& b);

	void moveLeft();
	void moveRight();
	void moveUp();
	void moveDown();

	int find(const sf::Vector2f& coords);

	const Field& getField() const { return m_field; }

private:
	Field m_field;
	BlockManager* m_block_manager;
};
#endif // !GAMEFIELD_H

