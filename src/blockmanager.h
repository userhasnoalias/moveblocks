#ifndef BLOCKMANAGER_H
#define BLOCKMANAGER_H

#include "block.h"
#include "eventmanager.h"
#include "gamefield.h"
//-----------------------------
#include <SFML/Graphics.hpp>
//-----------------------------
#include <vector>

using BlockContainer = std::vector<Block>;

class SharedContext;

class BlockManager
{
public:
	BlockManager(SharedContext* context);
	~BlockManager();

	void update();
	void draw();
	void checkForWin();

	void selectBlock(EventDetails* details);
	void moveLeft(EventDetails* details);
	void moveRight(EventDetails* details);
	void moveUp(EventDetails* details);
	void moveDown(EventDetails* details);

	SharedContext* getContext() { return m_context; }
	Block* getSelectedBlock() { return m_selected_block; }
	bool hasWon() const { return m_win; }
private:
	void initBlocks();
	void initTopBlocks();
	void createBlocks(EntityType type, int res_id, int count, bool movable = true);

	SharedContext* m_context;
	BlockContainer m_blocks;
	BlockContainer m_top_blocks;
	GameField m_field;
	Block* m_selected_block;
	bool m_win{};
};
#endif // !BLOCKMANAGER_H

