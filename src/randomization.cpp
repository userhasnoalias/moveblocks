#include "randomization.h"
//-----------------------------
#include <ctime>
#include <random>

void shuffleVec(std::vector<sf::Vector2i>& vec)
{
	static std::mt19937 mt{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };

	std::shuffle(vec.begin(), vec.end(), mt);
}